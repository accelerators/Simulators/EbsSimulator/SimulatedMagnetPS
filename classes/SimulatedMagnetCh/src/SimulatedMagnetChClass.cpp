/*----- PROTECTED REGION ID(SimulatedMagnetChClass.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        SimulatedMagnetChClass.cpp
//
// description : C++ source for the SimulatedMagnetChClass.
//               A singleton class derived from DeviceClass.
//               It implements the command and attribute list
//               and all properties and methods required
//               by the SimulatedMagnetCh once per process.
//
// project :     
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2022
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include "StrengthThread.h"
/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass.cpp

#include <tango/classes/SimulatedMagnetCh/SimulatedMagnetChClass.h>
//-------------------------------------------------------------------
/**
 *	Create SimulatedMagnetChClass singleton and
 *	return it in a C function for Python usage
 */
//-------------------------------------------------------------------
extern "C" {
#ifdef _TG_WINDOWS_

__declspec(dllexport)

#endif

	Tango::DeviceClass *_create_SimulatedMagnetCh_class(const char *name) {
		return SimulatedMagnetCh_ns::SimulatedMagnetChClass::init(name);
	}
}

namespace SimulatedMagnetCh_ns
{
//===================================================================
//	Initialize pointer for singleton pattern
//===================================================================
SimulatedMagnetChClass *SimulatedMagnetChClass::_instance = NULL;

//===================================================================
//	Class constants
//===================================================================
constexpr long LoopCoeffAttrib::X_DATA_SIZE;
//--------------------------------------------------------
/**
 * method : 		SimulatedMagnetChClass::SimulatedMagnetChClass(std::string &s)
 * description : 	constructor for the SimulatedMagnetChClass
 *
 * @param s	The class name
 */
//--------------------------------------------------------
SimulatedMagnetChClass::SimulatedMagnetChClass(std::string &s):Tango::DeviceClass(s)
{
	TANGO_LOG_INFO << "Entering SimulatedMagnetChClass constructor" << std::endl;
	set_default_property();
	get_class_property();
	write_class_property();

	/*----- PROTECTED REGION ID(SimulatedMagnetChClass::constructor) ENABLED START -----*/

  if(simulatorName.empty()) {
      std::cerr << "Fatal error: SimulatorName class property not defined" << std::endl;
    exit(0);
  }

  strengthThread = new StrengthThread(simulatorName,strengthMutex);

	/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass::constructor

	TANGO_LOG_INFO << "Leaving SimulatedMagnetChClass constructor" << std::endl;
}

//--------------------------------------------------------
/**
 * method : 		SimulatedMagnetChClass::~SimulatedMagnetChClass()
 * description : 	destructor for the SimulatedMagnetChClass
 */
//--------------------------------------------------------
SimulatedMagnetChClass::~SimulatedMagnetChClass()
{
	/*----- PROTECTED REGION ID(SimulatedMagnetChClass::destructor) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass::destructor

	_instance = NULL;
}


//--------------------------------------------------------
/**
 * method : 		SimulatedMagnetChClass::init
 * description : 	Create the object if not already done.
 *                  Otherwise, just return a pointer to the object
 *
 * @param	name	The class name
 */
//--------------------------------------------------------
SimulatedMagnetChClass *SimulatedMagnetChClass::init(const char *name)
{
	if (_instance == NULL)
	{
		try
		{
			std::string s(name);
			_instance = new SimulatedMagnetChClass(s);
		}
		catch (std::bad_alloc &)
		{
			throw;
		}
	}
	return _instance;
}

//--------------------------------------------------------
/**
 * method : 		SimulatedMagnetChClass::instance
 * description : 	Check if object already created,
 *                  and return a pointer to the object
 */
//--------------------------------------------------------
SimulatedMagnetChClass *SimulatedMagnetChClass::instance()
{
	if (_instance == NULL)
	{
		std::cerr << "Class is not initialised !!" << std::endl;
		exit(-1);
	}
	return _instance;
}



//===================================================================
//	Command execution method calls
//===================================================================
//--------------------------------------------------------
/**
 * method : 		OnClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *OnClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	TANGO_LOG_INFO << "OnClass::execute(): arrived" << std::endl;
	((static_cast<SimulatedMagnetCh *>(device))->on());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		OffClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *OffClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	TANGO_LOG_INFO << "OffClass::execute(): arrived" << std::endl;
	((static_cast<SimulatedMagnetCh *>(device))->off());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		FaultClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *FaultClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	TANGO_LOG_INFO << "FaultClass::execute(): arrived" << std::endl;
	((static_cast<SimulatedMagnetCh *>(device))->fault());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		ResetClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *ResetClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	TANGO_LOG_INFO << "ResetClass::execute(): arrived" << std::endl;
	((static_cast<SimulatedMagnetCh *>(device))->reset());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		CycleClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *CycleClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	TANGO_LOG_INFO << "CycleClass::execute(): arrived" << std::endl;
	((static_cast<SimulatedMagnetCh *>(device))->cycle());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		AbortCyclingClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *AbortCyclingClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	TANGO_LOG_INFO << "AbortCyclingClass::execute(): arrived" << std::endl;
	((static_cast<SimulatedMagnetCh *>(device))->abort_cycling());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		ErrorClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *ErrorClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	TANGO_LOG_INFO << "ErrorClass::execute(): arrived" << std::endl;
	((static_cast<SimulatedMagnetCh *>(device))->error());
	return new CORBA::Any();
}

//--------------------------------------------------------
/**
 * method : 		StartCyclingClass::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *StartCyclingClass::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	TANGO_LOG_INFO << "StartCyclingClass::execute(): arrived" << std::endl;
	((static_cast<SimulatedMagnetCh *>(device))->start_cycling());
	return new CORBA::Any();
}


//===================================================================
//	Properties management
//===================================================================
//--------------------------------------------------------
/**
 *	Method      : SimulatedMagnetChClass::get_class_property()
 * Description:  Get the class property for specified name.
 */
//--------------------------------------------------------
Tango::DbDatum SimulatedMagnetChClass::get_class_property(std::string &prop_name)
{
	for (unsigned int i=0 ; i<cl_prop.size() ; i++)
		if (cl_prop[i].name == prop_name)
			return cl_prop[i];
	//	if not found, returns  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedMagnetChClass::get_default_device_property()
 * Description:  Return the default value for device property.
 */
//--------------------------------------------------------
Tango::DbDatum SimulatedMagnetChClass::get_default_device_property(std::string &prop_name)
{
	for (unsigned int i=0 ; i<dev_def_prop.size() ; i++)
		if (dev_def_prop[i].name == prop_name)
			return dev_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedMagnetChClass::get_default_class_property()
 * Description:  Return the default value for class property.
 */
//--------------------------------------------------------
Tango::DbDatum SimulatedMagnetChClass::get_default_class_property(std::string &prop_name)
{
	for (unsigned int i=0 ; i<cl_def_prop.size() ; i++)
		if (cl_def_prop[i].name == prop_name)
			return cl_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedMagnetChClass::get_class_property()
 * Description:  Read database to initialize class property data members.
 */
//--------------------------------------------------------
void SimulatedMagnetChClass::get_class_property()
{
	/*----- PROTECTED REGION ID(SimulatedMagnetChClass::get_class_property_before) ENABLED START -----*/
	
	//	Initialize class property data members
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass::get_class_property_before
	//	Read class properties from database.
	cl_prop.push_back(Tango::DbDatum("SimulatorName"));

	//	Call database and extract values
	if (Tango::Util::instance()->_UseDb==true)
		get_db_class()->get_property(cl_prop);
	Tango::DbDatum	def_prop;
	int	i = -1;

	//	Try to extract SimulatorName value
	if (cl_prop[++i].is_empty()==false)	cl_prop[i]  >>  simulatorName;
	else
	{
		//	Check default value for SimulatorName
		def_prop = get_default_class_property(cl_prop[i].name);
		if (def_prop.is_empty()==false)
		{
			def_prop    >>  simulatorName;
			cl_prop[i]  <<  simulatorName;
		}
	}
	/*----- PROTECTED REGION ID(SimulatedMagnetChClass::get_class_property_after) ENABLED START -----*/
	
	//	Check class property data members init
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass::get_class_property_after

}

//--------------------------------------------------------
/**
 *	Method      : SimulatedMagnetChClass::set_default_property()
 * Description:  Set default property (class and device) for wizard.
 *                For each property, add to wizard property name and description.
 *                If default value has been set, add it to wizard property and
 *                store it in a DbDatum.
 */
//--------------------------------------------------------
void SimulatedMagnetChClass::set_default_property()
{
	std::string	prop_name;
	std::string	prop_desc;
	std::string	prop_def;
	std::vector<std::string>	vect_data;

	//	Set Default Class Properties
	prop_name = "SimulatorName";
	prop_desc = "Simulator device name";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		cl_def_prop.push_back(data);
		add_wiz_class_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_class_prop(prop_name, prop_desc);

	//	Set Default device Properties
	prop_name = "CurrentIndex";
	prop_desc = "Current index\n0 for main (if any)\n[0,1]..n for correctors";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "MagnetDevice";
	prop_desc = "Magnet device name (!not the corrector name!)";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "CyclingParams";
	prop_desc = "CyclingParams:\nI Floor (A)\nWait Floor (sec)\nI Max (A)\nWait Max (sec)";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
	prop_name = "RegulationParams";
	prop_desc = "RegulationParams\nCurrent Kp\nCurrent Ti (sec)\nVoltage Kp\nVoltage Ti (sec)\nImpedance (ohm)\nImpefance_I2";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedMagnetChClass::write_class_property()
 * Description:  Set class description fields as property in database
 */
//--------------------------------------------------------
void SimulatedMagnetChClass::write_class_property()
{
//	First time, check if database used
if (Tango::Util::_UseDb == false)
	return;

Tango::DbData	data;
std::string	classname = get_name();
std::string	header;

//	Put title
Tango::DbDatum	title("ProjectTitle");
std::string	str_title("");
title << str_title;
data.push_back(title);

//	Put Description
Tango::DbDatum	description("Description");
std::vector<std::string>	str_desc;
str_desc.push_back("A class to simulate Magnet power supplies (main and/or correctors)");
description << str_desc;
data.push_back(description);

//  Put inheritance
Tango::DbDatum	inher_datum("InheritedFrom");
std::vector<std::string> inheritance;
inheritance.push_back("TANGO_BASE_CLASS");
inher_datum << inheritance;
data.push_back(inher_datum);

//	Call database and and values
get_db_class()->put_property(data);
}

//===================================================================
//	Factory methods
//===================================================================

//--------------------------------------------------------
/**
 *	Method      : SimulatedMagnetChClass::device_factory()
 * Description:  Create the device object(s)
 *                and store them in the device list
 */
//--------------------------------------------------------
void SimulatedMagnetChClass::device_factory(const Tango::DevVarStringArray *devlist_ptr)
{
/*----- PROTECTED REGION ID(SimulatedMagnetChClass::device_factory_before) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass::device_factory_before

//	Create devices and add it into the device list
for (unsigned long i=0 ; i<devlist_ptr->length() ; i++)
{
	TANGO_LOG_DEBUG << "Device name : " << (*devlist_ptr)[i].in() << std::endl;
	device_list.push_back(new SimulatedMagnetCh(this, (*devlist_ptr)[i]));
}

//	Manage dynamic attributes if any
erase_dynamic_attributes(devlist_ptr, get_class_attr()->get_attr_list());

//	Export devices to the outside world
for (unsigned long i=1 ; i<=devlist_ptr->length() ; i++)
{
	//	Add dynamic attributes if any
	SimulatedMagnetCh *dev = static_cast<SimulatedMagnetCh *>(device_list[device_list.size()-i]);
	dev->add_dynamic_attributes();

	//	Check before if database used.
	if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
		export_device(dev);
	else
		export_device(dev, dev->get_name().c_str());
}

/*----- PROTECTED REGION ID(SimulatedMagnetChClass::device_factory_after) ENABLED START -----*/
	
  strengthThread->launch();

	/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass::device_factory_after
}
//--------------------------------------------------------
/**
 *	Method      : SimulatedMagnetChClass::attribute_factory()
 * Description:  Create the attribute object(s)
 *                and store them in the attribute list
 */
//--------------------------------------------------------
void SimulatedMagnetChClass::attribute_factory(std::vector<Tango::Attr *> &att_list)
{
	/*----- PROTECTED REGION ID(SimulatedMagnetChClass::attribute_factory_before) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass::attribute_factory_before
	//	Attribute : Current
	CurrentAttrib	*current = new CurrentAttrib();
	Tango::UserDefaultAttrProp	current_prop;
	//	description	not set for Current
	//	label	not set for Current
	current_prop.set_unit("A");
	//	standard_unit	not set for Current
	//	display_unit	not set for Current
	current_prop.set_format("%9.6f");
	//	max_value	not set for Current
	//	min_value	not set for Current
	//	max_alarm	not set for Current
	//	min_alarm	not set for Current
	//	max_warning	not set for Current
	//	min_warning	not set for Current
	//	delta_t	not set for Current
	//	delta_val	not set for Current
	current->set_default_properties(current_prop);
	//	Not Polled
	current->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	att_list.push_back(current);

	//	Attribute : Voltage
	VoltageAttrib	*voltage = new VoltageAttrib();
	Tango::UserDefaultAttrProp	voltage_prop;
	//	description	not set for Voltage
	//	label	not set for Voltage
	//	unit	not set for Voltage
	//	standard_unit	not set for Voltage
	//	display_unit	not set for Voltage
	//	format	not set for Voltage
	//	max_value	not set for Voltage
	//	min_value	not set for Voltage
	//	max_alarm	not set for Voltage
	//	min_alarm	not set for Voltage
	//	max_warning	not set for Voltage
	//	min_warning	not set for Voltage
	//	delta_t	not set for Voltage
	//	delta_val	not set for Voltage
	voltage->set_default_properties(voltage_prop);
	//	Not Polled
	voltage->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	att_list.push_back(voltage);

	//	Attribute : SetCurrentAverage
	SetCurrentAverageAttrib	*setcurrentaverage = new SetCurrentAverageAttrib();
	Tango::UserDefaultAttrProp	setcurrentaverage_prop;
	//	description	not set for SetCurrentAverage
	//	label	not set for SetCurrentAverage
	setcurrentaverage_prop.set_unit("mA");
	//	standard_unit	not set for SetCurrentAverage
	//	display_unit	not set for SetCurrentAverage
	//	format	not set for SetCurrentAverage
	//	max_value	not set for SetCurrentAverage
	//	min_value	not set for SetCurrentAverage
	//	max_alarm	not set for SetCurrentAverage
	//	min_alarm	not set for SetCurrentAverage
	//	max_warning	not set for SetCurrentAverage
	//	min_warning	not set for SetCurrentAverage
	//	delta_t	not set for SetCurrentAverage
	//	delta_val	not set for SetCurrentAverage
	setcurrentaverage->set_default_properties(setcurrentaverage_prop);
	//	Not Polled
	setcurrentaverage->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	att_list.push_back(setcurrentaverage);

	//	Attribute : LoopCoeff
	LoopCoeffAttrib	*loopcoeff = new LoopCoeffAttrib();
	Tango::UserDefaultAttrProp	loopcoeff_prop;
	//	description	not set for LoopCoeff
	//	label	not set for LoopCoeff
	//	unit	not set for LoopCoeff
	//	standard_unit	not set for LoopCoeff
	//	display_unit	not set for LoopCoeff
	//	format	not set for LoopCoeff
	//	max_value	not set for LoopCoeff
	//	min_value	not set for LoopCoeff
	//	max_alarm	not set for LoopCoeff
	//	min_alarm	not set for LoopCoeff
	//	max_warning	not set for LoopCoeff
	//	min_warning	not set for LoopCoeff
	//	delta_t	not set for LoopCoeff
	//	delta_val	not set for LoopCoeff
	loopcoeff->set_default_properties(loopcoeff_prop);
	//	Not Polled
	loopcoeff->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	att_list.push_back(loopcoeff);


	//	Create a list of static attributes
	create_static_attribute_list(get_class_attr()->get_attr_list());
	/*----- PROTECTED REGION ID(SimulatedMagnetChClass::attribute_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass::attribute_factory_after
}
//--------------------------------------------------------
/**
 *	Method      : SimulatedMagnetChClass::pipe_factory()
 * Description:  Create the pipe object(s)
 *                and store them in the pipe list
 */
//--------------------------------------------------------
void SimulatedMagnetChClass::pipe_factory()
{
	/*----- PROTECTED REGION ID(SimulatedMagnetChClass::pipe_factory_before) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass::pipe_factory_before
	/*----- PROTECTED REGION ID(SimulatedMagnetChClass::pipe_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass::pipe_factory_after
}
//--------------------------------------------------------
/**
 *	Method      : SimulatedMagnetChClass::command_factory()
 * Description:  Create the command object(s)
 *                and store them in the command list
 */
//--------------------------------------------------------
void SimulatedMagnetChClass::command_factory()
{
	/*----- PROTECTED REGION ID(SimulatedMagnetChClass::command_factory_before) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass::command_factory_before


	//	Command On
	OnClass	*pOnCmd =
		new OnClass("On",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pOnCmd);

	//	Command Off
	OffClass	*pOffCmd =
		new OffClass("Off",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pOffCmd);

	//	Command Fault
	FaultClass	*pFaultCmd =
		new FaultClass("Fault",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pFaultCmd);

	//	Command Reset
	ResetClass	*pResetCmd =
		new ResetClass("Reset",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pResetCmd);

	//	Command Cycle
	CycleClass	*pCycleCmd =
		new CycleClass("Cycle",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pCycleCmd);

	//	Command AbortCycling
	AbortCyclingClass	*pAbortCyclingCmd =
		new AbortCyclingClass("AbortCycling",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pAbortCyclingCmd);

	//	Command Error
	ErrorClass	*pErrorCmd =
		new ErrorClass("Error",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pErrorCmd);

	//	Command StartCycling
	StartCyclingClass	*pStartCyclingCmd =
		new StartCyclingClass("StartCycling",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(pStartCyclingCmd);

	/*----- PROTECTED REGION ID(SimulatedMagnetChClass::command_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass::command_factory_after
}

//===================================================================
//	Dynamic attributes related methods
//===================================================================

//--------------------------------------------------------
/**
 * method : 		SimulatedMagnetChClass::create_static_attribute_list
 * description : 	Create the a list of static attributes
 *
 * @param	att_list	the created attribute list
 */
//--------------------------------------------------------
void SimulatedMagnetChClass::create_static_attribute_list(std::vector<Tango::Attr *> &att_list)
{
	for (unsigned long i=0 ; i<att_list.size() ; i++)
	{
		std::string att_name(att_list[i]->get_name());
		transform(att_name.begin(), att_name.end(), att_name.begin(), ::tolower);
		defaultAttList.push_back(att_name);
	}

	TANGO_LOG_INFO << defaultAttList.size() << " attributes in default list" << std::endl;

	/*----- PROTECTED REGION ID(SimulatedMagnetChClass::create_static_att_list) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass::create_static_att_list
}


//--------------------------------------------------------
/**
 * method : 		SimulatedMagnetChClass::erase_dynamic_attributes
 * description : 	delete the dynamic attributes if any.
 *
 * @param	devlist_ptr	the device list pointer
 * @param	list of all attributes
 */
//--------------------------------------------------------
void SimulatedMagnetChClass::erase_dynamic_attributes(const Tango::DevVarStringArray *devlist_ptr, std::vector<Tango::Attr *> &att_list)
{
	Tango::Util *tg = Tango::Util::instance();

	for (unsigned long i=0 ; i<devlist_ptr->length() ; i++)
	{
		Tango::DeviceImpl *dev_impl = tg->get_device_by_name(((std::string)(*devlist_ptr)[i]).c_str());
		SimulatedMagnetCh *dev = static_cast<SimulatedMagnetCh *> (dev_impl);

		std::vector<Tango::Attribute *> &dev_att_list = dev->get_device_attr()->get_attribute_list();
		std::vector<Tango::Attribute *>::iterator ite_att;
		for (ite_att=dev_att_list.begin() ; ite_att != dev_att_list.end() ; ++ite_att)
		{
			std::string att_name((*ite_att)->get_name_lower());
			if ((att_name == "state") || (att_name == "status"))
				continue;
			std::vector<std::string>::iterator ite_str = find(defaultAttList.begin(), defaultAttList.end(), att_name);
			if (ite_str == defaultAttList.end())
			{
				TANGO_LOG_INFO << att_name << " is a UNWANTED dynamic attribute for device " << (*devlist_ptr)[i] << std::endl;
				Tango::Attribute &att = dev->get_device_attr()->get_attr_by_name(att_name.c_str());
				dev->remove_attribute(att_list[att.get_attr_idx()], true, false);
				--ite_att;
			}
		}
	}
	/*----- PROTECTED REGION ID(SimulatedMagnetChClass::erase_dynamic_attributes) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass::erase_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Method      : SimulatedMagnetChClass::get_attr_object_by_name()
 * Description:  returns Tango::Attr * object found by name
 */
//--------------------------------------------------------
Tango::Attr *SimulatedMagnetChClass::get_attr_object_by_name(std::vector<Tango::Attr *> &att_list, std::string attname)
{
	std::vector<Tango::Attr *>::iterator it;
	for (it=att_list.begin() ; it<att_list.end() ; ++it)
		if ((*it)->get_name()==attname)
			return (*it);
	//	Attr does not exist
	return NULL;
}


/*----- PROTECTED REGION ID(SimulatedMagnetChClass::Additional Methods) ENABLED START -----*/

/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetChClass::Additional Methods
} //	namespace
