//
// Cycling thread
//
#ifndef SCH_CYCLINGTHREAD_H
#define SCH_CYCLINGTHREAD_H

#include <tango/classes/SimulatedMagnetCh/SimulatedMagnetCh.h>

namespace SimulatedMagnetCh_ns {

class SimulatedMagnetCh;
class CyclingThread : public omni_thread, public Tango::LogAdapter {

public:

  // Constructor
  CyclingThread(SimulatedMagnetCh *,omni_mutex &,double nominalCurrent);
  void *run_undetached(void *);
  bool exitThread;
  bool startCycle;
  void getStatus(Tango::DevState& state,std::string& status);

  double nominalCurrent;

private:

  omni_mutex &mutex;
  SimulatedMagnetCh *ds;
  time_t cyclingStart;
  std::string cyclingStatus;
  Tango::DevState state;

  void wait(double current,int ms);
  void waitForCycle();
  bool write_current(double current,int ms);

};

} // end namespace

#endif //SCH_CYCLINGTHREAD_H
