//
// Acquisition thread for slow Bilt PS
//

#include "CyclingThread.h"

namespace SimulatedMagnetCh_ns {

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
CyclingThread::CyclingThread(SimulatedMagnetCh *l_ds,omni_mutex &m,double l_nominalCurrent):
        Tango::LogAdapter(l_ds), nominalCurrent(l_nominalCurrent), mutex(m), ds(l_ds) {

  exitThread = false;
  state = Tango::MOVING;
  cyclingStart = GetTicks();
  mutex.lock();
  cyclingStatus = "Cycling in progress";
  startCycle = false;
  mutex.unlock();
  start_undetached();

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *CyclingThread::run_undetached(void *) {

  INFO_STREAM << "CyclingThread::run(): Start thread" << std::endl;

  // Go to floor current
  if(!write_current(ds->cyclingParams[0],(int)(ds->cyclingParams[0+1]*1000.0)+5000))
    return nullptr;

  mutex.lock();
  cyclingStatus = "Cycling in progress: Floor current reached, wait for cycle";
  mutex.unlock();

  // Wait for start cycle
  waitForCycle();

  mutex.lock();
  cyclingStatus = "Cycling in progress: Performing cycle";
  mutex.unlock();

  for(size_t i=2;i<ds->cyclingParams.size() && !exitThread;i+=2) {
    if(!write_current(ds->cyclingParams[i],(int)(ds->cyclingParams[i+1]*1000.0)))
      return nullptr;
  }

  // When aborted, nominal must be restored
  if(!write_current(nominalCurrent,10000))
    return nullptr;

  // Reset state
  mutex.lock();
  cyclingStatus = "";
  state = Tango::ON;
  mutex.unlock();
  exitThread = true;
  return nullptr;

}

// ----------------------------------------------------------------------------------------
// Write current,wait and check error
// ----------------------------------------------------------------------------------------
bool CyclingThread::write_current(double current,int ms) {

  try {
    ds->write_Current(current);
    if(ms>0) wait(current,ms);
  } catch (Tango::DevFailed &e) {
    mutex.lock();
    ds->currentAtt->set_write_value(nominalCurrent);
    cyclingStatus = "Cycling failed: " + std::string(e.errors[0].desc);
    state = Tango::FAULT;
    mutex.unlock();
    exitThread = true;
    return false;
  }

  return true;

}

// ----------------------------------------------------------------------------------------
// Wait number of milliseconds
// ----------------------------------------------------------------------------------------
void CyclingThread::wait(double current,int ms) {

  if(exitThread)
    return;

  int maxRetry = 5;
  double delta = 2;
  double read_current;
  ds->self->read_attribute("Current") >> read_current;
  while(!exitThread && maxRetry>0 && (current-read_current)>delta ) {

    // 10 sec max to reach current (5*2sec)
    maxRetry--;
    int toSleep = 2000;
    while(toSleep>0 && !exitThread) {
      usleep(50000);
      toSleep -= 50;
    }
    if(!exitThread)
      ds->self->read_attribute("Current") >> read_current;

  }

  if(maxRetry==0) {
    Tango::Except::throw_exception(
            "CyclingError",
            "Current (" + std::to_string(current) + " A) cannot be reached",
            "CyclingThread::wait"
            );
  }

  // Sleep at current
  int toSleep = ms;
  while(toSleep>0 && !exitThread) {
    usleep(50000);
    toSleep -= 50;
  }

}

void CyclingThread::waitForCycle() {

  while(!exitThread && !startCycle) {
    usleep(50000);
  }

}

// ----------------------------------------------------------------------------------------
// Return status
// ----------------------------------------------------------------------------------------
void CyclingThread::getStatus(Tango::DevState& l_state,std::string& status) {
  mutex.lock();
  status = cyclingStatus;
  l_state = state;
  mutex.unlock();
}

} // end namespace
