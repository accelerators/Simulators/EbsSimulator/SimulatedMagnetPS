//
// Update strengths and send them to simulators
//

#include "StrengthThread.h"
#include <Quadrupole.h>
#include <DipoleQuadrupole.h>
#include <Sextupole.h>
#include <Octupole.h>
#include <SH3Magnet.h>
#include <SH5Magnet.h>

namespace SimulatedMagnetCh_ns
{

//-----------------------------------------------------------------------------
// Constructor
StrengthThread::StrengthThread(std::string& simulatorDevName,omni_mutex &l_mutex) : omni_thread(), mutex(l_mutex)
{

  db = Tango::Util::instance()->get_database();
  simuDS = new Tango::DeviceProxy(simulatorDevName);
  exitThread = false;

}

//-----------------------------------------------------------------------------
void StrengthThread::launch() {

  // Check that all channel are filled
  size_t totalChannel=0;
  bool ok = true;
  for(size_t i=0;i<magnets.size();i++) {
    for(size_t j=0;j<magnets[i].filled.size();j++) {
      if(!magnets[i].filled[j]) {
        ok = false;
        std::cerr << magnets[i].name << ": channel " << j << " not connected" << std::endl;
      }
    }
    totalChannel += magnets[i].currents.size();
  }

  if(!ok) {
      std::cerr << "Fatal error, not all channel connected" << std::endl;
    exit(0);
  }

  TANGO_LOG << magnets.size() << " magnets successfully initialised" << std::endl;
  TANGO_LOG << totalChannel << " channels successfully initialised" << std::endl;

  start_undetached();

}

//-----------------------------------------------------------------------------
// Add a magnet, return index in the global array
int StrengthThread::addMagnet(std::string& name,std::size_t channel,double impedance,double impedance_I2) {

  bool found = false;
  size_t i = 0;
  while(!found && i<magnets.size()) {
    found = magnets[i].name == name;
    if(!found) i++;
  }

  if(found) {
    if(channel<0 || channel>=magnets[i].filled.size()) {
        std::cerr << "Fatal Error: " << name << ", model fail: invalid channel index " << channel << std::endl;
      exit(0);
    }
    magnets[i].impedances[channel] = impedance;
    magnets[i].impedances_I2[channel] = impedance_I2;
    magnets[i].filled[channel] = true;
    return (int) i;
  }

  //cout << "Adding " << name << std::endl;

  // Get magnet properties
  Tango::DbData dbData;
  dbData.push_back(Tango::DbDatum("Type"));
  dbData.push_back(Tango::DbDatum("CrossTalkFactor"));
  dbData.push_back(Tango::DbDatum("SerialNumber"));
  dbData.push_back(Tango::DbDatum("Model"));
  dbData.push_back(Tango::DbDatum("Focusing"));
  db->get_device_property(name,dbData);
  std::string type = "None";
  double crossTalk = 1.0;
  std::string serial = "None";
  std::string model = "None";
  bool focusing = false;
  if(!dbData[0].is_empty()) dbData[0] >> type;
  if(!dbData[1].is_empty()) dbData[1] >> crossTalk;
  if(!dbData[2].is_empty()) dbData[2] >> serial;
  if(!dbData[3].is_empty()) dbData[3] >> model;
  if(!dbData[4].is_empty()) dbData[4] >> focusing;

  std::vector<ConfigItem> config = parseConfig(model);
  MagnetItem m;

  m.name = name;

  // Initialise model
  try {

    if (type == "Q") {

      auto *quad = new MagnetModel::Quadrupole();
      quad->init(focusing,
                 crossTalk * getConfigItemDouble(config,"scale"),
                 getConfigItem(config,"_path")+getConfigItem(config,"strength"),
                 getConfigItem(config,"_path")+getConfigItem(config,"params"),
                 serial);
      m.model = quad;

    } else if (type == "S") {

      auto *sextu = new MagnetModel::Sextupole();
      sextu->init(getConfigItem(config,"_path")+getConfigItem(config,"strength"),
                  getConfigItem(config,"_path")+getConfigItem(config,"params"),
                  serial);
      m.model = sextu;

    } else if (type == "O") {

      auto *octu = new MagnetModel::Octupole();
      octu->init(focusing,
                 crossTalk * getConfigItemDouble(config,"scale"),
                 getConfigItem(config,"_path")+getConfigItem(config,"strength"),
                 getConfigItem(config,"_path")+getConfigItem(config,"params"),
                 serial);
      m.model = octu;

    } else if (type == "DQ") {

      auto *dq = new MagnetModel::DipoleQuadrupole();
      dq->init(focusing,
               crossTalk * getConfigItemDouble(config,"scale"),
               getConfigItem(config,"_path")+getConfigItem(config,"strength"),
               getConfigItem(config,"_path")+getConfigItem(config,"params"),
               getConfigItem(config,"_path")+getConfigItem(config,"strength_h"),
               getConfigItem(config,"_path")+getConfigItem(config,"matrix"),
               serial);
      m.model = dq;

    } else if (type == "SH3") {

      auto *sh3 = new MagnetModel::SH3Magnet();
      sh3->init(crossTalk * getConfigItemDouble(config,"scale"),
                getConfigItem(config,"_path")+getConfigItem(config,"strength_h"),
                getConfigItem(config,"_path")+getConfigItem(config,"strength_v"),
                getConfigItem(config,"_path")+getConfigItem(config,"strength_sq"),
                getConfigItem(config,"_path")+getConfigItem(config,"matrix"));
      m.model = sh3;

    } else if (type == "SH5") {

      auto *sh5 = new MagnetModel::SH5Magnet();
      sh5->init(crossTalk*getConfigItemDouble(config,"scale"),
                getConfigItem(config,"_path")+getConfigItem(config,"strength"),
                getConfigItem(config,"_path")+getConfigItem(config,"strength_h"),
                getConfigItem(config,"_path")+getConfigItem(config,"strength_v"),
                getConfigItem(config,"_path")+getConfigItem(config,"strength_sq"),
                getConfigItem(config,"_path")+ getConfigItem(config,"matrix"));
      m.model = sh5;

    } else {
        std::cerr << "Fatal Error: MagnetIndex property wrongly defined for " << name << std::endl;
      exit(0);
    }
  } catch (std::invalid_argument& e) {
      std::cerr << "Fatal Error: " << name << ", model fail: " << e.what() << std::endl;
    exit(0);
  }

  size_t nbCurrents = (m.model->has_main_current()?1:0) + m.model->get_corrector_number();
  std::vector<std::string> strengthNames;
  m.model->get_strength_names(strengthNames);
  size_t nbStrengths = strengthNames.size();

  m.strengths.resize(nbStrengths,0.0);
  m.currents.resize(nbCurrents,0.0);
  m.setCurrents.resize(nbCurrents,0.0);
  m.voltages.resize(nbCurrents,0);
  m.impedances.resize(nbCurrents,0.0);
  m.impedances_I2.resize(nbCurrents,0.0);
  m.filled.resize(nbCurrents,false);

  if(channel<0 || channel>=m.filled.size()) {
      std::cerr << "Fatal Error: " << name << ", model fail: invalid channel index " << channel << std::endl;
    exit(0);
  }
  m.filled[channel] = true;
  m.impedances[channel] = impedance;
  m.impedances_I2[channel] = impedance_I2;

  // Compute pole names
  // srmag/['m'|'sext']-[fam]/c[xx]-[g]
  // sext for the 2 SH5 magnet of the injection zone
  // fam is not the model , only type of magnet and a number
  // xx is the cell 01 to 32
  // g the girder [a,b,c,d,e] , d is for the DQ between c and e girder
  //
  // Main magnetic component is the magnet name
  // H magnetic component srmag/hst-[fam]/c[xx]-[g]
  // V magnetic component srmag/hst-[fam]/c[xx]-[g]
  // SQ magnetic component srmag/sqp-[fam]/c[xx]-[g]

  std::vector<std::string> fields;
  std::vector<std::string> fmag;
  split(fields,name,'/');
  split(fmag,fields[1],'-');

  for(auto & strengthName : strengthNames) {

    if( strengthName == "Strength" ) {
      m.poleNames.emplace_back(name);
    } else if( strengthName == "Strength_H" ) {
      m.poleNames.emplace_back("srmag/hst-"+fmag[1]+"/"+fields[2]);
    } else if( strengthName == "Strength_V" ) {
      m.poleNames.emplace_back("srmag/vst-"+fmag[1]+"/"+fields[2]);
    } else if( strengthName == "Strength_SQ" ) {
      m.poleNames.emplace_back("srmag/sqp-"+fmag[1]+"/"+fields[2]);
    } else {
        std::cerr << "Fatal Error: " << name << ", model fail: invalid strength name " << strengthName << std::endl;
      exit(0);
    }

  }

  //for(auto & poleName : m.poleNames)
  //  TANGO_LOG << " => " << poleName << std::endl;
  magnets.emplace_back(m);


  return (int)(magnets.size()-1);

}

//-----------------------------------------------------------------------------
// Stop the thread
void StrengthThread::stop() {

  exitThread = true;

}

//-----------------------------------------------------------------------------
// Free allocated resource
StrengthThread::~StrengthThread() {

  for(auto & magnet : magnets)
    delete magnet.model;
  magnets.clear();

}

// -----------------------------------------------------------------------------------------
// Parse model config
// Retrieve calibration data filename and scale factor
std::vector<ConfigItem> StrengthThread::parseConfig(const std::string& model) {

  std::vector<ConfigItem> config;

  std::vector<Tango::DbDatum> dd;
  dd.emplace_back(Tango::DbDatum(model));
  db->get_property("EbsMagnet",dd);
  if(dd[0].is_empty()) {
    Tango::Except::throw_exception("ConfigError",
                                   "EbsMagnet free property: Configuration property not found for magnet model " + model,
                                   "Magnet::parseConfig");
  }

  std::vector<std::string> values;
  dd[0] >> values;
  for(auto & value : values) {
    std::vector<std::string> fields;
    split(fields,value,':');
    if(fields.size()!=2)
      Tango::Except::throw_exception("configError",
                                     "EbsMagnet free property: Invalid configuration property, name:value expected in " + value,
                                     "Magnet::parseConfig");
    ConfigItem item;
    item.name = fields[0];
    item.value = fields[1];
    config.emplace_back(item);
  }

  // Add path
  dd.clear();
  dd.emplace_back(Tango::DbDatum("_path"));
  db->get_property("EbsMagnet",dd);
  if(dd[0].is_empty()) {
    Tango::Except::throw_exception("ConfigError",
                                   "EbsMagnet free property: Configuration property _path not found",
                                   "Magnet::parseConfig");
  }

  ConfigItem item;
  item.name = "_path";
  std::string pathStr;
  dd[0] >> pathStr;
  item.value = pathStr;
  config.emplace_back(item);

  return config;

}

// -----------------------------------------------------------------------------------------
// Return configuration item
std::string StrengthThread::getConfigItem(std::vector<ConfigItem>& config,const std::string& name) {

  bool found = false;
  size_t i = 0;
  while(!found && i<config.size()) {
    found = strcasecmp(config[i].name.c_str(),name.c_str())==0;
    if(!found) i++;
  }
  if(!found)
    Tango::Except::throw_exception("ConfigError",
                                   "Configuration item '" + name +"' not found",
                                   "Magnet::parseConfig");
  return config[i].value;

}

// -----------------------------------------------------------------------------------------
// Return configuration item as double
double StrengthThread::getConfigItemDouble(std::vector<ConfigItem>& config,const std::string& name) {

  std::string s = getConfigItem(config,name);
  try {
    return std::stod(s);
  } catch (std::invalid_argument& e) {
    Tango::Except::throw_exception("ConfigError",
                                   "Configuration '" + name +"' number expected but got " + s,
                                   "Magnet::parseConfig");
  }

}

// -----------------------------------------------------------------------------------------
// Trim the given std::string
std::string trim(const std::string &s)
{
  auto start = s.begin();
  while (start != s.end() && std::isspace(*start))
    start++;
  auto end = s.end();
  do {
    end--;
  } while (std::distance(start, end) > 0 && std::isspace(*end));

  return std::string(start, end + 1);
}

// -----------------------------------------------------------------------------------------
// Split given std::string into tokens using specified separator
void StrengthThread::split(std::vector<std::string> &tokens, const std::string &text, char sep) {

  size_t start = 0, end = 0;
  tokens.clear();
  while ((end = text.find(sep, start)) != std::string::npos) {
    tokens.push_back(trim(text.substr(start, end - start)));
    start = end + 1;
  }
  tokens.push_back(trim(text.substr(start)));

}

//-----------------------------------------------------------------------------
void StrengthThread::getCurrent(int magId,int channel,double& current,double& setpoint) {
  if(magId<0) {
    current = NAN;
    setpoint = NAN;
    return;
  }
  current = magnets[magId].currents[channel];
  setpoint = magnets[magId].setCurrents[channel];
}

//-----------------------------------------------------------------------------
void StrengthThread::getVoltage(int magId,int channel,double& voltage) {
  if(magId<0) {
    voltage = NAN;
    return;
  }
  voltage = magnets[magId].voltages[channel];
}

//-----------------------------------------------------------------------------
void StrengthThread::setCurrent(int magId,int channel,double current) {
  if(magId<0)
    return;
  mutex.lock();
  magnets[magId].setCurrents[channel] = current;
  mutex.unlock();
}

//-----------------------------------------------------------------------------
void *StrengthThread::run_undetached(void*) {

  TANGO_LOG << "start StrengthThread\n";

  while(!exitThread) {

    time_t t0 = GetTicks();

    // Compute currents
    for(auto & magnet : magnets) {

      size_t start = 0;
      if(magnet.model->has_main_current()) {
        double delta = 0.55 * (magnet.setCurrents[0] - magnet.currents[0]);
        if(delta>30) delta = 30; // 30A/sec max
        magnet.currents[0] += delta;
        magnet.voltages[0] = magnet.currents[0] * magnet.impedances[0] / 0.88; // Statum 88% efficiency
        start = 1;
      }

      for(size_t i=start;i<magnet.setCurrents.size();i++) {
        magnet.currents[i] = magnet.setCurrents[i];
        magnet.voltages[i] = magnet.currents[i] * magnet.impedances[i];
      }

    }


    // Compute strengths
    Tango::DeviceData cmdIn;
    std::vector<double> v_d;
    std::vector<std::string> v_s;

    mutex.lock();
    for(auto & magnet : magnets) {
      magnet.model->compute_strengths(MAGNET_RIGIDITY_INV,magnet.currents,magnet.strengths);
      for(size_t j=0;j<magnet.poleNames.size();j++) {
        v_d.emplace_back(magnet.strengths[j]);
        v_s.emplace_back(magnet.poleNames[j]);
      }
    }
    mutex.unlock();

    cmdIn.insert(v_d, v_s);
    try {
      simuDS->command_inout("SetMagnetStrengthAll", cmdIn);
    } catch (Tango::DevFailed &e) {
      TANGO_LOG << "SetMagnetStrength failed:" << e.errors[0].desc << std::endl;
    }

    time_t t1 = GetTicks();

    int toSleep = 1000 - (int)(t1-t0);
    while(toSleep>0 && !exitThread) {
      usleep(100000);
      toSleep -= 100;
    }

  }

  return nullptr;

}


}	//	namespace
