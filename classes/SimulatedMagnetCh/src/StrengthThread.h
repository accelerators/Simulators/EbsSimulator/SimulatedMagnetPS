//
// Update strengths and send them to simulators
//

#ifndef SIMULTATEDMAGNETPS_STRENGTHTHREAD_H
#define SIMULTATEDMAGNETPS_STRENGTHTHREAD_H


#include <tango/tango.h>
#include <tango/classes/SimulatedMagnetCh/SimulatedMagnetCh.h>
#include <Magnet.h>

namespace SimulatedMagnetCh_ns
{

typedef struct ConfigItem {
  std::string name;
  std::string value;
} ConfigItem;

typedef struct MagnetItem {

  std::string name;
  MagnetModel::Magnet *model;   // Magnet model
  std::vector<double> strengths;
  std::vector<double> currents;
  std::vector<double> setCurrents;
  std::vector<double> voltages;
  std::vector<double> impedances;
  std::vector<double> impedances_I2;
  std::vector<bool> filled;
  std::vector<std::string> poleNames;

} MagnetItem;

class StrengthThread : public omni_thread
{

private:

  omni_mutex    &mutex;
  void 					*run_undetached(void *arg);
  bool           exitThread;

  Tango::Database *db;
  Tango::DeviceProxy *simuDS;
  std::vector<MagnetItem> magnets;

  std::vector<ConfigItem> parseConfig(const std::string& model);
  std::string getConfigItem(std::vector<ConfigItem>& config,const std::string& name);
  double getConfigItemDouble(std::vector<ConfigItem>& config,const std::string& name);
  static void split(std::vector<std::string> &tokens, const std::string &text, char sep);

public:

  StrengthThread(std::string& simulatorDevName,omni_mutex &mutex);
  ~StrengthThread();
  int addMagnet(std::string& name,std::size_t channel,double impedance,double impedance_I2);
  void setCurrent(int magId,int channel,double current);
  void getCurrent(int magId,int channel,double& current,double& setpoint);
  void getVoltage(int magId,int channel,double& voltage);
  void stop();
  void launch();

};

}	//	namespace

#endif //SIMULTATEDMAGNETPS_STRENGTHTHREAD_H
