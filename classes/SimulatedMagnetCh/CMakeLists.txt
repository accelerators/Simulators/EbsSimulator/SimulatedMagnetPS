###############################################################################
project(SimulatedMagnetCh
    VERSION 
        ${_VERSION}
    DESCRIPTION
        ""
    LANGUAGES
        CXX
)


# Tango Class dependencies (inheritance from parent Tango Class) declaration
set(LINK_LIBRARIES_CLASS
)
###############################################################################
# Build library
include(build-tango-class)
