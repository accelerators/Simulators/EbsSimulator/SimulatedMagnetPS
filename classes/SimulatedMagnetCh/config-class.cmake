# USER (TANGO CLASS) CONFIGURATION
# Here update local configuration of ${DEVICE_CLASS_TARGET}
#
# target_sources(${DEVICE_CLASS_TARGET}
#    PRIVATE
#        src/impl.cpp
# )
#
# target_link_libraries(${DEVICE_CLASS_TARGET}
#    PRIVATE
#        custom::lib
# )

target_sources(${DEVICE_CLASS_TARGET}
   PRIVATE
       src/CyclingThread.cpp
       src/StrengthThread.cpp
)

target_link_libraries(${DEVICE_CLASS_TARGET}
   PRIVATE
   Eigen3::Eigen
   MagnetModel::MagnetModel
)
