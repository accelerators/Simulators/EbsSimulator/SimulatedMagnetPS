/*----- PROTECTED REGION ID(SimulatedMagnetCh.h) ENABLED START -----*/
//=============================================================================
//
// file :        SimulatedMagnetCh.h
//
// description : Include file for the SimulatedMagnetCh class
//
// project :     
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2022
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef SimulatedMagnetCh_H
#define SimulatedMagnetCh_H

#include <ctime>

#define MAGNET_RIGIDITY 20.0138495839
#define MAGNET_RIGIDITY_INV (1.0/MAGNET_RIGIDITY)

/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetCh.h
#include <tango/tango.h>


#ifdef TANGO_LOG
	// cppTango after c934adea (Merge branch 'remove-cout-definition' into 'main', 2022-05-23)
	// nothing to do
#else
	// cppTango 9.3-backports and older
	#define TANGO_LOG       cout
	#define TANGO_LOG_INFO  cout2
	#define TANGO_LOG_DEBUG cout3
#endif // TANGO_LOG

/**
 *  SimulatedMagnetCh class description:
 *    A class to simulate Magnet power supplies (main and/or correctors)
 */


namespace SimulatedMagnetCh_ns
{
/*----- PROTECTED REGION ID(SimulatedMagnetCh::Additional Class Declarations) ENABLED START -----*/

//	Additional Class Declarations
class StrengthThread;
class CyclingThread;

extern time_t GetTicks();

/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetCh::Additional Class Declarations

class SimulatedMagnetCh : public TANGO_BASE_CLASS
{

/*----- PROTECTED REGION ID(SimulatedMagnetCh::Data Members) ENABLED START -----*/

public:
  Tango::DevState psState;
  StrengthThread *strengthThread;
  CyclingThread *cyclingThread;
  omni_mutex cyclingMutex;
  int magnetID;
  Tango::DeviceProxy *self;
  double lastCurrent;
  Tango::WAttribute *currentAtt;

  void write_Current(double val);

/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetCh::Data Members

//	Device property data members
public:
	//	CurrentIndex:	Current index
	//  0 for main (if any)
	//  [0,1]..n for correctors
	Tango::DevLong	currentIndex;
	//	MagnetDevice:	Magnet device name (!not the corrector name!)
	std::string	magnetDevice;
	//	CyclingParams:	CyclingParams:
	//  I Floor (A)
	//  Wait Floor (sec)
	//  I Max (A)
	//  Wait Max (sec)
	std::vector<Tango::DevDouble>	cyclingParams;
	//	RegulationParams:	RegulationParams
	//  Current Kp
	//  Current Ti (sec)
	//  Voltage Kp
	//  Voltage Ti (sec)
	//  Impedance (ohm)
	//  Impefance_I2
	std::vector<Tango::DevDouble>	regulationParams;

//	Attribute data members
public:
	Tango::DevDouble	*attr_Current_read;
	Tango::DevDouble	*attr_Voltage_read;
	Tango::DevDouble	*attr_SetCurrentAverage_read;
	Tango::DevDouble	*attr_LoopCoeff_read;

//	Constructors and destructors
public:
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	SimulatedMagnetCh(Tango::DeviceClass *cl,std::string &s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	SimulatedMagnetCh(Tango::DeviceClass *cl,const char *s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device name
	 *	@param d	Device description.
	 */
	SimulatedMagnetCh(Tango::DeviceClass *cl,const char *s,const char *d);
	/**
	 * The device object destructor.
	 */
	~SimulatedMagnetCh();


//	Miscellaneous methods
public:
	/*
	 *	will be called at device destruction or at init command.
	 */
	void delete_device();
	/*
	 *	Initialize the device
	 */
	virtual void init_device();
	/*
	 *	Read the device properties from database
	 */
	void get_device_property();
	/*
	 *	Always executed method before execution command method.
	 */
	virtual void always_executed_hook();


//	Attribute methods
public:
	//--------------------------------------------------------
	/*
	 *	Method      : SimulatedMagnetCh::read_attr_hardware()
	 * Description:  Hardware acquisition for attributes.
	 */
	//--------------------------------------------------------
	virtual void read_attr_hardware(std::vector<long> &attr_list);
	//--------------------------------------------------------
	/*
	 *	Method      : SimulatedMagnetCh::write_attr_hardware()
	 * Description:  Hardware writing for attributes.
	 */
	//--------------------------------------------------------
	virtual void write_attr_hardware(std::vector<long> &attr_list);

/**
 *	Attribute Current related methods
 *
 *
 *	Data type:  Tango::DevDouble
 *	Attr type:	Scalar
 */
	virtual void read_Current(Tango::Attribute &attr);
	virtual void write_Current(Tango::WAttribute &attr);
	virtual bool is_Current_allowed(Tango::AttReqType type);
/**
 *	Attribute Voltage related methods
 *
 *
 *	Data type:  Tango::DevDouble
 *	Attr type:	Scalar
 */
	virtual void read_Voltage(Tango::Attribute &attr);
	virtual bool is_Voltage_allowed(Tango::AttReqType type);
/**
 *	Attribute SetCurrentAverage related methods
 *
 *
 *	Data type:  Tango::DevDouble
 *	Attr type:	Scalar
 */
	virtual void read_SetCurrentAverage(Tango::Attribute &attr);
	virtual bool is_SetCurrentAverage_allowed(Tango::AttReqType type);
/**
 *	Attribute LoopCoeff related methods
 *
 *
 *	Data type:  Tango::DevDouble
 *	Attr type:	Spectrum max = 6
 */
	virtual void read_LoopCoeff(Tango::Attribute &attr);
	virtual bool is_LoopCoeff_allowed(Tango::AttReqType type);


	//--------------------------------------------------------
	/**
	 *	Method      : SimulatedMagnetCh::add_dynamic_attributes()
	 * Description:  Add dynamic attributes if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_attributes();




//	Command related methods
public:
	/**
	 *	Command State related method
	 * Description:  This command gets the device state (stored in its device_state data member) and returns it to the caller.
	 *
	 *	@returns Device state
	 */
	virtual Tango::DevState dev_state();
	/**
	 *	Command On related method
	 * Description:  Switch ON
	 *
	 */
	virtual void on();
	virtual bool is_On_allowed(const CORBA::Any &any);
	/**
	 *	Command Off related method
	 * Description:  Switch OFF
	 *
	 */
	virtual void off();
	virtual bool is_Off_allowed(const CORBA::Any &any);
	/**
	 *	Command Fault related method
	 * Description:  Swith to Fault state
	 *
	 */
	virtual void fault();
	virtual bool is_Fault_allowed(const CORBA::Any &any);
	/**
	 *	Command Reset related method
	 *
	 *
	 */
	virtual void reset();
	virtual bool is_Reset_allowed(const CORBA::Any &any);
	/**
	 *	Command Cycle related method
	 * Description:  Perform cycle (goes to max and return to nominal)
	 *
	 */
	virtual void cycle();
	virtual bool is_Cycle_allowed(const CORBA::Any &any);
	/**
	 *	Command AbortCycling related method
	 * Description:  Abort cycling
	 *
	 */
	virtual void abort_cycling();
	virtual bool is_AbortCycling_allowed(const CORBA::Any &any);
	/**
	 *	Command Error related method
	 * Description:  Put the ds in error
	 *
	 */
	virtual void error();
	virtual bool is_Error_allowed(const CORBA::Any &any);
	/**
	 *	Command StartCycling related method
	 * Description:  Start cycling process
	 *
	 */
	virtual void start_cycling();
	virtual bool is_StartCycling_allowed(const CORBA::Any &any);


	//--------------------------------------------------------
	/**
	 *	Method      : SimulatedMagnetCh::add_dynamic_commands()
	 * Description:  Add dynamic commands if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_commands();

/*----- PROTECTED REGION ID(SimulatedMagnetCh::Additional Method prototypes) ENABLED START -----*/

//	Additional Method prototypes

/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetCh::Additional Method prototypes
};

/*----- PROTECTED REGION ID(SimulatedMagnetCh::Additional Classes Definitions) ENABLED START -----*/

//	Additional Classes Definitions

/*----- PROTECTED REGION END -----*/	//	SimulatedMagnetCh::Additional Classes Definitions

}	//	End of namespace

#endif   //	SimulatedMagnetCh_H
