# Project SimulatedMagnetPS

Tango c++ class to simulate EBS magnets (and correctors). It computes strength from magnet power supply currents and send them to the simulators based on pyAT.

## Cloning

```
git clone git@gitlab.esrf.fr:accelerators/Simulators/EbsSimulator/SimulatedMagnetPS.git
```

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies 

* Tango Controls 9 or higher
* omniORB release 4 or higher
* libzmq
* MagnetModel library

To compile, your project structure must looks like 

#### Toolchain Dependencies 

* C++11 compliant compiler.
* CMake 3.0 or greater is required to perform the build. 

### Build

Instructions on building the project.

CMake example: 

```bash
cd project_name
mkdir build
cd build
cmake ../
make
```

#
